import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StorageManagerService {
  constructor() {}

  set(key: string, object: object | string): void {
    localStorage.setItem(key, JSON.stringify(object));
  }
  get(key: string): unknown | null{
    return localStorage.getItem(key)
      ? JSON.parse(localStorage.getItem(key) as string)
      : null;
  }
  destroy(key: string): void {
    localStorage.removeItem(key);
  }
}

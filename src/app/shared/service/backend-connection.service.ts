import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/module/auth/service/auth.service';
import { environment } from '../../../environments/environment';
import { StorageManagerService } from './storage-manager.service';
import { tap } from 'rxjs/operators';
import { MessageService } from './message.service';
import { Params } from '@angular/router';

export class BackendConnection {
  url = environment.backendUrl;

  expiredTokenHandler = (error: HttpErrorResponse): void => {
    console.log('error', error);

    if (error?.status === 401) {
      this.storageManagerService.destroy('usuario');
      this.authService.logout(true);
    }
  }

  constructor(
    protected http: HttpClient,
    protected storageManagerService: StorageManagerService,
    protected authService: AuthService,
  ) { }

  /* getAll<T>(endpoint: string): Observable<T> {
    return this.http.get<T>(this.url + endpoint).pipe(
      tap({
        error: this.expiredTokenHandler
      }))
  } */

  // método que recibe como parametro la api principal y los parametros de paginación
  getAll<T>(endpoint: string, queryParams: any = null): Observable<T> {
    if (queryParams !== null) {
      console.log('query params', queryParams)
      const params = new HttpParams()
        .append('page', `${queryParams.page}`)
        .append('count', `${queryParams.count}`)
        .append('sortOrder', `${queryParams.sortOrder}`)
        .append('sortType', `${queryParams.sortType}`)
        .append('criteria', `${queryParams.criteria}`);
      console.log('params', params.keys(), queryParams);
      return this.http.get<T>(this.url + endpoint, { params }).pipe(
        tap({
          error: this.expiredTokenHandler
        }))
    }
    return this.http.get<T>(this.url + endpoint).pipe(
      tap({
        error: this.expiredTokenHandler
      }))
  }

  getOne<T>(endpoint: string, id: string, otro?: string): Observable<T> {
    return this.http
      .get<T>(this.url + endpoint + '/' + id,);
  }

  post<T>(endpoint: string, payload: any): Observable<T> {
    return this.http
      .post<T>(this.url + endpoint, payload);
  }

  put<T>(endpoint: string, id: string, payload: object): Observable<T> {
    return this.http
      .put<T>(this.url + endpoint + '/' + id, payload);
  }

  delete<T>(endpoint: string, entity: string): Observable<T> {
    return this.http
      .delete<T>(this.url + endpoint + '/' + entity);
  }
}

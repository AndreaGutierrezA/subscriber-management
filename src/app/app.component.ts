import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './module/auth/service/auth.service';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  unlogged$: Observable<boolean> = of(false);

  constructor(
    private authService: AuthService,
    private router: Router,
  ) { 
    this.unlogged$ = this.authService.unlogged
  }
  
  logout() {
    this.authService.logout();
    this.router.navigate(['login'])
  }
}

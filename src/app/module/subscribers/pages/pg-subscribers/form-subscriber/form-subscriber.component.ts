import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { IDatumC } from 'src/app/core/model/interface';
import { CountriesService } from '../../../service/countries.service';

@Component({
  selector: 'app-form-subscriber',
  templateUrl: './form-subscriber.component.html',
  styleUrls: ['./form-subscriber.component.scss']
})
export class FormSubscriberComponent implements OnInit {
  // @ts-ignore
  data: any;
  // @ts-ignore
  habilitarId: boolean;
  // @ts-ignore
  validateForm: FormGroup;
  // @ts-ignore
  validateFormEnviar: FormGroup;

  countries: IDatumC[] = [];
  // @ts-ignore
  loading: boolean;
  constructor(
    private fb: FormBuilder,
    private modal: NzModalRef,
    private countriesService: CountriesService) {
  }
  ngOnInit(): void {
    this.getListCountries()
    this.crearFormulario();
    this.validateFormEnviar.valueChanges.subscribe((value) => {
      console.log('valid fom', this.validateFormEnviar.valid);

      if (this.validateFormEnviar.valid) {
        this.modal.updateConfig({
          nzOkDisabled: false
        });
      } else {
        this.modal.updateConfig({
          nzOkDisabled: true
        });
      }
    });
    console.log(this.habilitarId);

  }
  getListCountries(params: any = null) {
    this.loading = true;
    console.log('countries', params);
    const initialParams = {
      page: 1,
      count: 300,
      sortOrder: 'Name',
      sortType: 0,
      criteria: ''
    }
    this.countriesService.getListCountries(params !== null ? params : initialParams).
      subscribe((resp) => {
        this.countries = resp?.Data;
        this.loading = false;
        console.log('countries', this.countries);
      }, (error) => {
        console.log(error);
        this.loading = false;
      })
  }
  crearFormulario(): void {
    // formulario interno input
    this.validateForm = this.fb.group({
      id: [{ disabled: this.habilitarId, value: this.data ? this.data?.Id : '' }, []],
      Name: [this.data ? this.data?.Name : '', [Validators.required]],
      Email: [this.data ? this.data?.Email : '', [Validators.email, Validators.required]],
      CountryCode: [this.data ? this.data?.CountryCode : '', [Validators.required]],
      PhoneNumber: [this.data ? this.data?.PhoneNumber : '', [Validators.required, Validators.pattern(/^([0-9])*$/)]],
      JobTitle: [this.data ? this.data?.JobTitle : '', []],
      Area: [this.data ? this.data?.Area : '', []],
      Topics: [[], []],

    })
    // formulario externo tabla
    this.validateFormEnviar = this.fb.group({
      Subscribers: this.fb.array([], [Validators.required]),

    })
  }
  //Agregar datos a la tabla
  createItem(Name: string = '',
    Email: string = '',
    CountryCode: string = '',
    PhoneNumber: string = '',
    JobTitle: string = '', Area: string = ''): FormGroup {
    return this.fb.group({
      id: [{ disabled: this.habilitarId, value: this.data ? this.data?.Id : '' }, []],
      Name: [Name, [Validators.required]],
      Email: [Email, [Validators.email, Validators.required]],
      CountryCode: [CountryCode, [Validators.required]],
      PhoneNumber: [PhoneNumber, [Validators.required, Validators.pattern(/^([0-9])*$/)]],
      JobTitle: [JobTitle, []],
      Area: [Area, []],
      Topics: [[], []],
    });
  }
  addItem(): void {
    console.log('agregar');
    const Subscribers = this.validateFormEnviar.get('Subscribers') as FormArray;
    Subscribers.push(
      this.createItem(
        this.validateForm.get('Name')?.value,
        this.validateForm.get('Email')?.value,
        this.validateForm.get('CountryCode')?.value,
        this.validateForm.get('PhoneNumber')?.value,
        this.validateForm.get('JobTitle')?.value,
        this.validateForm.get('Area')?.value,
      )
    );
    this.validateForm.reset();

  }
  remove(i: number) {
    console.log('eliminar');
    const controls = this.validateFormEnviar.get('Subscribers');
    (controls as FormArray).removeAt(i);
  }

}

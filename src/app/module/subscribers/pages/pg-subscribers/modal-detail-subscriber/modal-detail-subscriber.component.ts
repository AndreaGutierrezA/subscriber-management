import { Component, OnInit } from '@angular/core';
import { IDatum } from 'src/app/core/model/interface';

@Component({
  selector: 'app-modal-detail-subscriber',
  templateUrl: './modal-detail-subscriber.component.html',
  styleUrls: ['./modal-detail-subscriber.component.scss']
})
export class ModalDetailSubscriberComponent implements OnInit {
// @ts-ignore
  data: IDatum
  constructor() { }

  ngOnInit(): void {
    console.log('datos subcriptor',this.data);
    
  }

}

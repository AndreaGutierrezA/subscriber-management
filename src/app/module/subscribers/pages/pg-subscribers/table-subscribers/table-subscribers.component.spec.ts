import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableSubscribersComponent } from './table-subscribers.component';

describe('TableSubscribersComponent', () => {
  let component: TableSubscribersComponent;
  let fixture: ComponentFixture<TableSubscribersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableSubscribersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableSubscribersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

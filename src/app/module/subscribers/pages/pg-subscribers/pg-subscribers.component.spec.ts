import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PgSubscribersComponent } from './pg-subscribers.component';

describe('PgSubscribersComponent', () => {
  let component: PgSubscribersComponent;
  let fixture: ComponentFixture<PgSubscribersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PgSubscribersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PgSubscribersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { IDatumC } from 'src/app/core/model/interface';
import { AuthService } from 'src/app/module/auth/service/auth.service';
import { CountriesService } from '../../service/countries.service';

@Component({
  selector: 'app-pg-countries',
  templateUrl: './pg-countries.component.html',
  styleUrls: ['./pg-countries.component.scss']
})
export class PgCountriesComponent implements OnInit {

  countries: IDatumC[] = [];
    // @ts-ignore
  loading: boolean;
  // @ts-ignore
  countriesSubscription: Subscription;
  total: number = 0
  searchValue: any = '';
  constructor(
    private countriesService: CountriesService,
    private router: Router,
    private authService: AuthService
    ) { 
      this.authService.setLogout(true)
    }

  ngOnInit(): void {
    this.getListCountries();
  }
  /* getListCountries() {
    this.loading = true;
    this.countriesService.getListCountries().
      subscribe((resp) => {
        this.countries = resp?.Data;
        this.loading = false;
        console.log('countries', this.countries);
      })
  } */
  getListCountries(params: any = null) {
    this.loading = true;
    console.log('getListCountries', params);
    const initialParams = {
      page: 1,
      count: 10,
      sortOrder: 'Name',
      sortType: 0,
      criteria: this.searchValue
    }
    this.countriesSubscription = this.countriesService.getListCountries(params !== null ? params : initialParams).
      subscribe((resp) => {
        this.countries = resp?.Data;
        console.log('countries', this.countries);
        this.total = resp?.Count;
        console.log('this.total', this.total);
        this.loading = false;
        // cancelacion de la suscripción
        this.countriesSubscription.unsubscribe();
      }, (error) => {
        console.log(error);
        this.loading = false;
      })
  }
  onAtras(){
    this.router.navigate(['subscribers'])
  }
 

}

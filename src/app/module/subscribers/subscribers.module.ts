import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscribersRoutingModule } from './subscribers-routing.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgZorroModule } from 'src/app/shared/modules/ng-zorro/ng-zorro.module';

import { PgSubscribersComponent } from './pages/pg-subscribers/pg-subscribers.component';
import { TableSubscribersComponent } from './pages/pg-subscribers/table-subscribers/table-subscribers.component';
import { ModalDetailSubscriberComponent } from './pages/pg-subscribers/modal-detail-subscriber/modal-detail-subscriber.component';
import { FormSubscriberComponent } from './pages/pg-subscribers/form-subscriber/form-subscriber.component';
import { PgCountriesComponent } from './pages/pg-countries/pg-countries.component';
import { TableCountriesComponent } from './pages/pg-countries/table-countries/table-countries.component';

@NgModule({
  declarations: [
    PgSubscribersComponent,
    TableSubscribersComponent,
    ModalDetailSubscriberComponent,
    FormSubscriberComponent,
    PgCountriesComponent,
    TableCountriesComponent
  ],
  imports: [
    CommonModule,
    SubscribersRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgZorroModule
  ],
})
export class SubscribersModule { }

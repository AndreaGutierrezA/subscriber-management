import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../service/auth.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { StorageManagerService } from 'src/app/shared/service/storage-manager.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  mensajes: { [name: string]: { titulo: string; contenido: string }; } = {
    success: { titulo: 'Login exitoso', contenido: 'Bienvenido, has ingresado al sistema correctamente.' },
    error: { titulo: 'Error inesperado', contenido: 'Estamos experimentando problemas en la aplicación, por favor intenta de nuevo mas tarde.' },
    fail: { titulo: 'Login ha fallado', contenido: 'Por favor intente nuevamente, asegurese de proporcionar las credenciales correctamente.' },
  }
  validateForm: FormGroup = new FormGroup({});

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private notificationService: NzNotificationService,
    private router: Router,
    private storageManagerService: StorageManagerService
  ) {
    this.authService.setLogout(false)
  }

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.validateForm = this.fb.group({
      UserName: ['patata', [Validators.required]],
      password: ['MrPotat0', [Validators.required]]
    });
  }

  submitForm(): void {
    this.authService.login(this.validateForm.value).subscribe(
      (resp) => {
        this.storageManagerService.set('token', `Bear ${resp?.Token}`);
        this.notificationService.success(
          this.mensajes['success'].titulo,
          this.mensajes['success'].contenido
        );
        this.router.navigate(['subscribers'])
      },
      (error) => {
        console.log('error', error);
        this.handleError('error')
      }
    );
  }

  private handleError(errorData: any) {
    // notificar el error
    this.notificationService.error(
      this.mensajes['error'].titulo,
      this.mensajes['error'].contenido
    );
    console.error(errorData);
  }
}

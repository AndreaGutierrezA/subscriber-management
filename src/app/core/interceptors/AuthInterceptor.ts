import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { StorageManagerService } from 'src/app/shared/service/storage-manager.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private storageManagerService: StorageManagerService
    ) { 
  }

  intercept<T>(
    req: HttpRequest<T>,
    next: HttpHandler
  ): Observable<HttpEvent<T>> {
    const token = this.storageManagerService.get('token') as string;
    const authReq = req.clone({
      headers: req.headers.set('Content-Type', 'application/json')
      .set('Authorization', token || 'aaa' )
    });
    return next.handle(authReq);
  }
}

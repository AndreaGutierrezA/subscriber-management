import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { StorageManagerService } from 'src/app/shared/service/storage-manager.service';


@Injectable({
  providedIn: 'root'
})
export class SessionGuard implements CanActivate {
  constructor(
    private router: Router,
    private storageManagerService: StorageManagerService) {

  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.checkLocalStorage();
  }

  checkLocalStorage(): boolean {
    try {

      const token: string = this.storageManagerService.get('token') as string
      console.log('token',token);
      
      if (!token) {
        console.log('if',token);
        this.router.navigate(['/', 'login'])
      }
      return true

    } catch (e) {
      console.log('Algo sucedio ?? ', e);
      return false
    }

  }
  
}
